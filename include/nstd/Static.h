#ifndef NSTD_COMPILE_TIME_H_INC
#define NSTD_COMPILE_TIME_H_INC

#include <type_traits>
#include <stdexcept>

namespace nstd {
namespace Static {

/**
 * Exception class that indicates a given value is not in the map
 */
class key_not_found_error : public std::domain_error {
public:
    ///default constructor
    key_not_found_error() : std::domain_error(
            "Key not found in static lookup table"
    ) {}
};

//prototypes:

template <unsigned N, class KeyGen, class Fn>
class Map;

template <unsigned N, class Fn>
class Table;

//include implementation files:
#include <nstd/bits/Static/static_table_impl.h>
#include <nstd/bits/Static/Map.h>
#include <nstd/bits/Static/Table.h>

} //namespace Static
} //namespace nstd

#endif
