/**
 * A statically initialized lookup table.
 * 
 * The Fn type is a function type that maps an unsigned index to a
 * value.
 * 
 * This class can be thought of as an optimized version of static_map
 * where KeyGen is the identity function.
 * 
 * Calling an object of type Fn MUST be a constexpr operation, and Fn
 * MUST be a constexpr default constructable function object.
 * 
 * To illustrate:
 * 
 * \code{.cpp}
 * 
 * //this may be used with either constructor
 * template <typename value_type>
 * struct Fn {
 *     constexpr value_type operator()(unsigned);
 * };
 * 
 * \endcode
 * 
 * \tparam N The number of entries in the lookup table
 * \tparam Fn A type that maps an unsigned key to a value
 * 
 */
template <unsigned N, class Fn>
class Table {
public:
    //public typedefs
	///The type of the keys in the table (for compatibility with static_map)
    typedef unsigned key_type;
    ///The type of the values in the table
    typedef decltype((Fn{})(0U)) value_type;
	///Shorthand form for the type of this object
    typedef Table<N, Fn> this_type;
    ///The number of values in the table
    static constexpr unsigned length = N;
private:

	//decrease verbosity
	typedef static_table_impl impl;
	
    ///the actual lookup table
    const value_type table[N];

	#ifndef DOXYGEN
    //this does the heavy lifting of generating the table
    template <unsigned... Is>
    constexpr Table(impl::seq<Is...>, Fn func) :
        //initialize table - generate values    
        table{ func(Is)... }
    {
        //
    }
    
    //make this private to maintain consistency & compatibility with
    //Map.  It's more intuitive if there's just *one* set of
    //rules to follow - even if that set of rules prevents one sort
    //of behavior :/
    
    explicit constexpr Table(Fn func) :
		Table(impl::gen_seq<N>(), func)
	{
		//
	}
    #endif
public:

    /**
     * Construct a table
     */
    constexpr Table() : 
        Table(impl::gen_seq<N>(), Fn()) 
    {
		//
	}

	/**
	 * Look up an index in the table.
	 * 
	 * This function is constexpr, so it can be computed at compile
	 * time if i is itself constexpr.
	 * 
	 * The time complexity of this operation is O(1).
	 * 
	 * This function is not bounds checked.
	 * 
	 * \param i An index
	 * \return The value at i.
	 */
    constexpr value_type operator[](unsigned i) {
        return table[i];
    }
    	
    /**
	 * Look up an index in the table.
	 * 
	 * This function is exactly the same as operator[]().  It is provided
	 * for convenience and compatibility with Map.
	 * 
	 * This function is constexpr, so it can be computed at compile
	 * time if i is itself constexpr.
	 * 
	 * The time complexity of this operation is O(1).
	 * 
	 * This function is not bounds checked.
	 * 
	 * \param i An index
	 * \return The value at i.
	 */
    constexpr value_type AtIndex(unsigned i) {
		return table[i];
	}
	
	/**
	 * Find the index of a key
	 * 
	 * This function is simply the identity function.  It is provided
	 * for interface compatibility with Map.
	 * 
	 * \param i The index
	 * \return i
	 */
	constexpr unsigned KeyAtIndex(unsigned i) {
		return i;
	}

	///if the keys are sorted - provided for compatibility with static_map
    static constexpr bool sorted = true; //always true (0...N always sorted)
};

