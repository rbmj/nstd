/**
 * Implementation container for map, table
 * 
 * This class holds components of map/table that are not dependent
 * on some or all of their template parameters.
 *
 * They are moved into a separate class to avoid excess code generation.
 * 
 * Everything in this class is private and either a type or static.
 * map and table are friends of this class so that they
 * can access these internals (a sort of private namespace).
 */
class static_table_impl {
	//make map, table friends so it can use our private members
    template <unsigned N, class KeyGen, class Fn>
    friend class Map;
    template <unsigned N, class Fn>
    friend class Table;
private:
    /**
     * A compile-time sequence of unsigned integers.
     * 
     * \tparam Is Represents the sequence of unsigned integers
     */
    template <unsigned... Is> struct seq {};
	
	#ifdef DOXYGEN
	/**
	 * A recursive type that generates a sequence from 0 to N
	 * 
	 * gen_seq<N> will be a subclass of seq<0, ... N>
	 * 
	 * \tparam N The upper bound on the sequence
	 */
	template <unsigned N>
	struct gen_seq<N> {};
	#else
	//real code for this:
    template <unsigned N, unsigned... Is>
    struct gen_seq : gen_seq<N-1, N-1, Is...> {};

    template <unsigned... Is>
    struct gen_seq<0, Is...> : seq<Is...> {};
    #endif

	/**
	 * A type-traits class that detects if T can be compared with <
	 * 
	 * \tparam T The type to check
	 */
    template <class T>
	class comparable {
	private:
		///Indicates the first case
		typedef char a;
		///Indicates the second case
		typedef struct {char x[2];} b;
		/**
		 * First case.
		 * 
		 * If the result of comparing two values of type T using < is
		 * implicitly convertible to bool, then this case will be a
		 * better match for test(0) than test(...).
		 */
		template <class C = T>
		static a test(
			typename std::enable_if<
				std::is_convertible<
					decltype(std::declval<C>() < std::declval<C>()),
					bool
				>::value,
				int
			>::type
		);
		/**
		 * Second case.
		 * 
		 * This case will always match test(0), but will be a worse
		 * match than the first case, assuming that the first case is
		 * not eliminated due to SFINAE.
		 */
		template <class C = T>
		static b test(...);
	public:
		/**
		 * Whether T can be compared using <
		 * 
		 * This will be true if the result of a comparison of two values
		 * of type T is implicitly convertible to bool.
		 */
		static constexpr bool value = (sizeof(test(0)) == sizeof(a));
	};
    
    /** 
     * A constexpr function that gets the midpoint of a range
     * 
     * \param begin The start of the range
     * \param end The end of the range
     * \return The midpoint of the range [begin, end)
     */
    static constexpr unsigned midpoint(unsigned begin, unsigned end) {
		return begin + ((end - begin) / 2);
	}
};

